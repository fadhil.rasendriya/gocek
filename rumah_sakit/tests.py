from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import RumahSakit, Dokter
from .forms import *
from . import views

import datetime

# Create your tests here.
class TestRoute(TestCase):
    def test_url_main(self):
        response = Client().get('/rumah-sakit/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used_main(self):
        response = resolve('/rumah-sakit/')
        self.assertEqual(response.func, views.index)

    def test_url_tambah_rs(self):
        response = Client().get('/rumah-sakit/tambah-rs/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_tambah_rs(self):
        response = resolve('/rumah-sakit/tambah-rs/')
        self.assertEqual(response.func, views.tambahRS)    
    
    def test_url_update_rs(self):
        test = 'test'
        RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test)
        response = Client().get('/rumah-sakit/update-rumah-sakit/1/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used_update_rs(self):
        test = 'test'
        RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test)
        response = resolve('/rumah-sakit/update-rumah-sakit/1/')
        self.assertEqual(response.func, views.updateRS)    
        
    def test_url_detail_rs(self):
        test = 'test'
        RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test)
        response = Client().get('/rumah-sakit/detail-rumah-sakit/1/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used_detail_rs(self):
        test = 'test'
        RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test)
        response = resolve('/rumah-sakit/detail-rumah-sakit/1/')
        self.assertEqual(response.func, views.detailRS)    
        
    def test_url_tambah_dokter_rs(self):
        test = 'test'
        RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test)
        response = Client().get('/rumah-sakit/detail-rumah-sakit/1/tambah-dokter/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used_tambah_dokter_rs(self):
        test = 'test'
        RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test)
        response = resolve('/rumah-sakit/detail-rumah-sakit/1/tambah-dokter/')
        self.assertEqual(response.func, views.tambahDokter)


class TestActivity(TestCase):
    def test_model_rumah_sakit_can_create(self):
        test = 'test'
        RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test)
        counter = RumahSakit.objects.all().count()
        self.assertEqual(counter, 1)
    
    def test_model_dokter_can_create(self):
        test = 'test'
        test_jam_buka = datetime.time(0,0,0)
        test_jam_tutup = datetime.time(1,0,0)
        rs = RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test)
        Dokter.objects.create(rs = rs, nama_dokter = test, spesialis = test, layanan_start = test_jam_buka, layanan_end = test_jam_tutup, phone_number = 1)

