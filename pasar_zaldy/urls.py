from django.urls import path
from . import views

app_name = 'pasar_zaldy'

urlpatterns = [
    path('', views.pasar_mainpage, name='pasar_mainpage'),
    path('tambah/', views.pasar_tambah, name='pasar_tambah'),
    path('<int:id>/tambahpenjual/', views.penjual_tambah, name='penjual_tambah'),
    path('<int:id>/updatepasar/', views.pasar_update, name='pasar_update'),
    path('detail/<int:id>/', views.pasar_detail, name='pasar_detail'),
    
]

