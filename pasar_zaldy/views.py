from django.shortcuts import render, get_object_or_404, render,  redirect
from .models import Penjual, Pasar
from .forms import PasarForms, PenjualForms
from django.contrib import messages


def pasar_mainpage(request):
    queryset = Pasar.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, "pasar_mainpage.html", context)

def pasar_tambah(request):
    form = PasarForms(request.POST or None)
    if form.is_valid(): #validator
        form.save()
        return redirect('/pasar/')
    context = {
        'form' : form
    }
    return render(request, "pasar_form.html", context)

def penjual_tambah(request, id=1):
    if request.method == "POST":
        form = PenjualForms(request.POST or None)
        if form.is_valid():
            penjualValid = Penjual(pasarPenjual=Pasar.objects.get(pk=id), 
            namaPenjual=form.data['namaPenjual'],
            kontakPenjual = form.data['kontakPenjual'],
            jenisJualan = form.data['jenisJualan'])
            penjualValid.save()  
        return redirect('/pasar/detail/'+str(id))
    form = PenjualForms()
    return render(request, "penjual_form.html", {'penjual': form})

def pasar_detail(request, id):
    obj = get_object_or_404(Pasar, id=id)
    kumpulanPenjual = Penjual.objects.filter(pasarPenjual=obj)
    return render(request, 'pasar_detail.html', {"penjual":kumpulanPenjual, "obj":obj})

def pasar_update(request, id):
    pasar = Pasar.objects.get(pk=id)
    form = PasarForms(instance=pasar)
    if request.method == 'POST':
        form = PasarForms(request.POST, instance=pasar)
        if form.is_valid():
            form.save()
            return redirect('/pasar/detail/'+str(id))
    context = {
        'form' : form
    }
    return render(request, "pasar_update_form.html", context)
