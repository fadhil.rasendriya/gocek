from django.test import TestCase, Client
from django.urls import resolve, reverse
from .apps import PasarZaldyConfig
from .models import Pasar,Penjual
import datetime, tempfile
from .forms import (PasarForms, PenjualForms)
# Create your tests here.

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(PasarZaldyConfig.name, "pasar_zaldy")


class TestFormIsValid(TestCase):
    def test_user_already_exists(self):
        data = {
            'namaPasar': 'testclient',
            'jambukaPasar': '08:00',
            'jamtutupPasar': '08:30',
            'hotlinePasar': '3123131',
            'fotoPasar': 'test123',
            'alamatPasar': 'test123',
            'statuscovidPasar': 'Zona Kuning',
        }
        fors = PasarForms(data)
        self.assertTrue(fors.is_valid())
        self.assertTrue(fors.save)
        

class TestURLRouting(TestCase):
    def test_mainpage_url_is_exist(self):
        response = Client().get('/pasar/')
        self.assertEqual(response.status_code, 200)

    def test_tambah_url_is_exist(self):
        response = Client().get('/pasar/tambah/')
        self.assertEqual(response.status_code, 200)

    def test_detail_url_is_exist(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        activity = Pasar.objects.create(namaPasar="Pasar Senin", hotlinePasar="09421421", 
        statuscovidPasar = 'Aman', alamatPasar = "dsadadasdadasd", jambukaPasar= datetime.time(0,0,0),
        jamtutupPasar = datetime.time(1,0,0), fotoPasar=image )
        get = activity.id
        response = Client().get('/pasar/detail/' +str(get)+ '/')
        self.assertEqual(response.status_code, 200)
    
    def test_update_url_is_exist(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        activity = Pasar.objects.create(namaPasar="Pasar Senin", hotlinePasar="09421421", 
        statuscovidPasar = 'Aman', alamatPasar = "dsadadasdadasd", jambukaPasar= datetime.time(0,0,0),
        jamtutupPasar = datetime.time(1,0,0), fotoPasar=image )
        get = activity.id
        response = Client().get('/pasar/' +str(get)+ '/updatepasar/')
        self.assertEqual(response.status_code, 200)

    def test_mainpage_using_template(self):
        response = Client().get('/pasar/')
        self.assertTemplateUsed(response, 'pasar_mainpage.html')

    def test_tambah_using_template(self):
        response = Client().get('/pasar/tambah/')
        self.assertTemplateUsed(response, 'pasar_form.html')

    def test_tambahpenjual_using_template(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        activity = Pasar.objects.create(namaPasar="Pasar Senin", hotlinePasar="09421421", 
        statuscovidPasar = 'Aman', alamatPasar = "dsadadasdadasd", jambukaPasar= datetime.time(0,0,0),
        jamtutupPasar = datetime.time(1,0,0), fotoPasar=image )
        get = activity.id
        response = Client().get('/pasar/' +str(get)+ '/tambahpenjual/')
        self.assertEqual(response.status_code, 200)

class TestPageContent(TestCase):
    def test_mainpage_in_template(self):
        response = Client().get('/pasar/')
        konten_html = response.content.decode('utf8')
        self.assertIn('Pasar', konten_html)
        self.assertIn('GOCEK', konten_html)
        self.assertIn('Cek Tempat', konten_html)
        self.assertIn('Bagi Informasi', konten_html)

    def test_tambahpage_in_template(self):
        response = Client().get('/pasar/tambah/')
        konten_html = response.content.decode('utf8')
        self.assertIn('Nama Pasar:', konten_html)
        self.assertIn('Hotline Pasar:', konten_html)
        self.assertIn('Jam Buka Pasar:', konten_html)
        self.assertIn('Jam Tutup Pasar:', konten_html)
        self.assertIn('GOCEK', konten_html)
        self.assertIn('Cek Tempat', konten_html)
        self.assertIn('Bagi Informasi', konten_html)


class TestSTRPasar(TestCase):
    def test_pasar_str(self):
        pas = Pasar.objects.create(namaPasar='margonda')
        self.assertEqual(str(pas), 'margonda')

    def test_penjual_str(self):
        pas = Penjual.objects.create(namaPenjual='anto')
        self.assertEqual(str(pas), 'anto')
