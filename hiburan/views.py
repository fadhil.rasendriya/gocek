from django.shortcuts import render, get_object_or_404, render,  redirect
from .forms import CreateHiburan, CreateFoto
from .models import Hiburan, Fotos
from . import models, forms


# Create your views here.

def index(request):
    # hiburan = Hiburan.objects.all()
    
    print(Hiburan.objects.all())
    return render(request, 'hiburan/index.html', {"hiburan" : Hiburan.objects.all()})

def addTempat(request):
    form = CreateHiburan()
    if request.method == 'POST':
        form = forms.CreateHiburan(request.POST)
        if form.is_valid():
            hbrn = models.Hiburan()
            hbrn.nama = form.cleaned_data['nama']
            hbrn.status = form.cleaned_data['status']
            hbrn.budget = form.cleaned_data['budget']
            hbrn.kontak = form.cleaned_data['kontak']
            hbrn.alamat = form.cleaned_data['alamat']
            hbrn.jam_buka = form.cleaned_data['jam_buka']
            hbrn.jam_tutup = form.cleaned_data['jam_tutup']
            hbrn.foto = form.cleaned_data['foto']
            hbrn.save()
            return redirect('/hiburan')
    return render(request, 'hiburan/addTempat.html', {'form':form})

def addFoto(request, id):
    if request.method == "POST":
        form = CreateFoto(request.POST)
        if form.is_valid():
            fotonya = Fotos(tempat_hiburan=Hiburan.objects.get(pk=id))
            fotonya.link_foto = form.data['link_foto']
            fotonya.save()  
        return redirect('/hiburan/detail/'+str(id))
    form = CreateFoto()
    return render(request, "hiburan/addFoto.html", {'data': form})


def detail(request, id):
    obj = get_object_or_404(Hiburan, id=id)
    fotos = Fotos.objects.filter(tempat_hiburan=obj)
    return render(request, 'hiburan/detail.html', {"obj":obj, "fotos":fotos})

def update(request, id):
    hbrn = Hiburan.objects.get(pk=id)
    form = CreateHiburan()
    if request.method == 'POST':
        data = request.POST
        hbrn.status = data['status']
        hbrn.budget = data['budget']
        hbrn.kontak = data['kontak']
        hbrn.alamat = data['alamat']
        hbrn.save()
        return redirect('/hiburan/detail/'+str(id))
    context = {
        'form' : form
    }
    return render(request, "hiburan/update.html", context)

