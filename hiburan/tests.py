from django.test import TestCase, Client
from django.urls import resolve, reverse
from .apps import HiburanConfig
from .models import Hiburan, Fotos
import datetime, tempfile
from .forms import (CreateHiburan,CreateFoto)
# Create your tests here.

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(HiburanConfig.name, "hiburan")


# class TestFormIsValid(TestCase):
#     def test_user_already_exists(self):
#         data = {
#             'nama': 'testclient',
#             'jam_buka': '08:00',
#             'jam_tutup': '08:30',
#             'budget': '10000',
#             'foto': 'test123',
#             'alamat': 'test123',
#             'status': 'Zona Kuning',
#             'kontak': '080000000000',
#         }
#         form = CreateHiburan(data)
#         self.assertTrue(form.is_valid())
#         self.assertTrue(form.save)
        

class TestURLRouting(TestCase):
    def test_mainpage_url_is_exist(self):
        response = Client().get('/hiburan/')
        self.assertEqual(response.status_code, 200)

    def test_addTempat_url_is_exist(self):
        response = Client().get('/hiburan/addTempat/')
        self.assertEqual(response.status_code, 200)

    def test_detail_url_is_exist(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        activity = Hiburan.objects.create(nama="Dufan", kontak="09421421", 
        status = 'Aman', alamat = "dsadadasdadasd", jam_buka= "08::00",
        jam_tutup = "08:30", foto=image )
        get = activity.id
        response = Client().get('/hiburan/detail/' +str(get)+ '/')
        self.assertEqual(response.status_code, 200)
    
    def test_update_url_is_exist(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        activity = Hiburan.objects.create(nama="Dufan", kontak="09421421", 
        status = 'Aman', alamat = "dsadadasdadasd", jam_buka= "08::00",
        jam_tutup = "08:30", foto=image )
        get = activity.id
        response = Client().get('/hiburan/' +str(get)+ '/update/')
        self.assertEqual(response.status_code, 200)

    def test_mainpage_using_template(self):
        response = Client().get('/hiburan/')
        self.assertTemplateUsed(response, 'hiburan/index.html')

    def test_addTempat_using_template(self):
        response = Client().get('/hiburan/addTempat/')
        self.assertTemplateUsed(response, 'hiburan/addTempat.html')

    def test_addFoto_using_template(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        activity = Hiburan.objects.create(nama="Dufan", kontak="09421421", 
        status = 'Aman', alamat = "dsadadasdadasd", jam_buka= "08::00",
        jam_tutup = "08:30", foto=image )
        get = activity.id
        response = Client().get('/hiburan/' +str(get)+ '/addFoto/')
        self.assertEqual(response.status_code, 200)


class TestSTRHiburan(TestCase):
    def test_hiburan_str(self):
        hib = Hiburan.objects.create(nama='Dufan')
        self.assertEqual(str(hib), 'Dufan')

    def test_foto_str(self):
        hib = Fotos.objects.create(link_foto='anto')
        self.assertEqual(str(hib), 'anto')
