from django.urls import path
from . import views


app_name = 'hiburan'

urlpatterns = [
    path('', views.index, name='index'),
    path('addTempat/', views.addTempat, name='addTempat'),
    path('<int:id>/addFoto/', views.addFoto, name='addFoto'),
    path('detail/<int:id>/', views.detail, name='detail'),
    path('<int:id>/update/', views.update, name='update'),

]
